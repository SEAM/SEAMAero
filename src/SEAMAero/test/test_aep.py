__author__ = 's127504'

import numpy as np
import unittest

from openmdao.main.api import Assembly

from SEAMAero.SEAM_AEP import SEAM_AEP, SEAM_PowerCurve, SEAMAEP

# data from Kenneth's model
AEP = 14.50         #[GWh / year]
Total_AEP = 290.04  #[GWh]

class AEP_Assembly(Assembly):

    def configure(self):

        self.add('power',SEAM_PowerCurve())
        self.driver.workflow.add('power')

        self.add('aep',SEAM_AEP())
        self.driver.workflow.add('aep')

        self.connect('power.wind_curve', 'aep.wind_curve')
        self.connect('power.power_curve', 'aep.power_curve')

        self.power.n_wsp = 26
        self.power.min_wsp = 0  #[m/s]
        self.power.max_wsp = 25 #[m/s]
        self.power.air_density = 1.225      # [kg / m^3]
        self.power.turbulence_int = 0.1     # Fraction
        self.power.rated_power = 3          # [MW]
        self.power.rotor_diameter = 101          # [m]
        self.power.max_Cp = 0.49            # Fraction
        self.power.gearloss_const = 0.01    # Fraction
        self.power.gearloss_var = 0.014     # Fraction
        self.power.genloss = 0.03          # Fraction
        self.power.convloss = 0.03         # Fraction

        self.aep.WeibullInput = True        # 1(true) or 0 (false) if true WeiA and WeiC overrules MeanWSP. If false MeanWSP is used with Rayleigh distribution
        self.aep.WeiA_input = 11.       #[m/s]
        self.aep.WeiC_input = 2.00       #[-]


        self.aep.NYears = 20

class SEAM_AEPTestCase(unittest.TestCase):

    def setUp(self):
        self.top = AEP_Assembly()
        self.top.run()

    def test_SEAM_AEP(self):

        self.assertAlmostEqual(self.top.aep.total_aep, 290.043156004, places = 8)

if __name__ == '__main__':
    unittest.main()




