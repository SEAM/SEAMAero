
# unit aep_unit

from math import pow
import numpy as np
from scipy.optimize import minimize

from openmdao.main.api import Component, Assembly
from openmdao.lib.datatypes.api import Float, Array, Bool, Int


class SEAM_PowerCurve(Component):

    n_wsp = Int(iotype = 'in', desc = 'number of wind speed bins')
    min_wsp = Float(0.0, iotype = 'in', units = 'm/s', desc = 'min wind speed')
    max_wsp = Float(iotype = 'in', units = 'm/s', desc = 'max wind speed')
    mean_wsp = Float(iotype = 'in', units = 'm/s', desc = 'mean wind speed')    # [m/s]
    air_density = Float(iotype = 'in', units = 'kg/m**3', desc = 'density of air') # [kg / m^3]
    turbulence_int = Float(iotype = 'in', desc = '')
    rated_power = Float(iotype = 'in', units = 'MW', desc = 'rated power')
    rotor_diameter = Float(iotype = 'in', units = 'm', desc = 'diameter')
    max_Cp = Float(iotype = 'in', desc = 'max CP')
    gearloss_const = Float(iotype = 'in', desc = 'Gear loss constant')
    gearloss_var = Float(iotype = 'in', desc = 'Gear loss variable')
    genloss = Float(iotype = 'in', desc = 'Generator loss')
    convloss = Float(iotype = 'in', desc = 'Converter loss')
    max_tipspeed = Float(iotype='in', desc='Maximum tip speed')

    # Outputs
    rated_wind_speed = Float(units = 'm / s', iotype='out', desc='wind speed for rated power')
    ideal_power_curve = Array(iotype='out', units='kW', desc='total power before losses and turbulence')
    power_curve = Array(iotype='out', units='kW', desc='total power including losses and turbulence')
    wind_curve = Array(iotype='out', units='m/s', desc='wind curve associated with power curve')
    #07/09/2015 addition for DriveSE
    rated_torque = Float(units='N*m',iotype='out', desc='torque at rated power')
    rated_speed = Float(iotype='out', units='rpm', desc='rated rotor speed')

    def execute(self):

        rated_power = self.rated_power * 1.e6

        wsp = np.linspace(self.min_wsp, self.max_wsp, self.n_wsp)
        P_aero = 0.5 * self.air_density * (np.pi * (self.rotor_diameter/2.)**2) * wsp**3 * self.max_Cp

        P_gear = np.maximum(0., P_aero - self.gearloss_const * rated_power - P_aero * self.gearloss_var)
        P_gen = P_gear * (1 - self.genloss)
        P_conv = P_gen * (1 - self.convloss)
        P_raw = np.minimum(P_conv, rated_power)

        # TurbulenceCorrection
        NormDist = np.zeros((self.n_wsp-1, self.n_wsp))

        sigma = wsp*self.turbulence_int
        NDist = lambda x, my, std: (1./(np.sqrt(2*np.pi)*std))*np.exp(-(x-my)*(x-my)/(2.*std**2))

        ProbSum = np.zeros(P_conv.shape[0])
        for i in range(self.n_wsp - 1):
         for j in range(self.n_wsp):
             NormDist[i, j] = NDist(wsp[j], wsp[i+1], sigma[i+1])
             ProbSum[i] = ProbSum[i] + NormDist[i, j]

        P_turb = np.zeros(self.n_wsp)
        for i in range(1, self.n_wsp):
            for j in range(self.n_wsp):
                P_turb[i] = P_turb[i] + P_raw[j] * NormDist[i-1, j] / ProbSum[i-1]

        self.P_aero = P_aero * 1.e-3
        self.ideal_power_curve = P_raw * 1.e-3
        self.power_curve = P_turb * 1.e-3
        self.wind_curve = wsp

        # estimate rated torque 07/09/2015
        wsp_index = list(P_raw).index(rated_power)
        self.rated_wind_speed = self.wind_curve[wsp_index]
        self.rated_speed = (self.max_tipspeed/(0.5*self.rotor_diameter)) * (60.0 / (2*np.pi))
        self.rated_torque = rated_power/(((1-self.genloss)*(1-self.convloss)*(1-self.gearloss_const-self.gearloss_var)) *(self.rated_speed*(np.pi/30.)))

    def plot(self, fig):
        """
        function to generate Bokeh plot for web GUI.

        Also callable from an ipython notebook

        parameters
        ----------
        fig: object
            Bokeh bokeh.plotting.figure object

        returns
        -------
        fig: object
            Bokeh bokeh.plotting.figure object
        """
        try:
            # formatting
            fig.title = 'Power curve'
            fig.xaxis[0].axis_label = 'Wind speed [m/s]'
            fig.yaxis[0].axis_label = 'Power production [kW]'

            # fatigue, ultimate and final thickness line plots
            fig.line(self.wind_curve, self.ideal_power_curve, line_color='orange',
                                        line_width=3,
                                        legend='Ideal')
            fig.line(self.wind_curve, self.power_curve, line_color='green',
                                        line_width=3,
                                        legend='With turbulence')

            fig.legend[0].orientation = 'bottom_right'
        except:
            pass

        return fig


class SEAM_AEP(Component):

    power_curve = Array(iotype='in', units='kW', desc='total power including losses and turbulence')
    wind_curve = Array(iotype='in', units='m/s', desc='wind curve associated with power curve')
    WeibullInput = Bool(True, iotype = 'in', desc='If True WeiA and WeiC overrules MeanWSP.'
                                                  'If False MeanWSP is used with Rayleigh distribution')
    WeiA_input = Float(iotype = 'in', units='m/s', desc = '')
    WeiC_input = Float(iotype = 'in', desc='')
    NYears = Float(iotype = 'in', desc='Operating years')  # move this to COE calculation

    aep = Float(iotype = 'out', units='mW*h', desc='Annual energy production in mWh')
    total_aep = Float(iotype = 'out', units='mW*h', desc='AEP for total years of production')

    def execute(self):

        wsp = self.wind_curve
        n_wsp = self.power_curve.shape[0]
        P_turb = self.power_curve

        if not self.WeibullInput:
            self.WeibullC = 2.0
            self.WeibullA = 1.1284*self.MeanWSP
        else:
            self.WeibullC = self.WeiC_input
            self.WeibullA = self.WeiA_input

        NHours = np.zeros(n_wsp)
        NHours[0] = 8760*((1-np.exp(-np.exp(self.WeibullC*np.log((wsp[0]+0.5)/self.WeibullA)))))
        for i in range(1, n_wsp):
            NHours[i] = 8760. * ((1 - np.exp(-np.exp(self.WeibullC * np.log((wsp[i] + 0.5) / self.WeibullA)))) - \
                                 (1 - np.exp(-np.exp(self.WeibullC * np.log((wsp[i] - 0.5) / self.WeibullA)))))

        self.aep_pr_wsp = P_turb * NHours
        AEPSum = np.sum(self.aep_pr_wsp * 1.e-6)

        self.aep = AEPSum
        self.total_aep = AEPSum * self.NYears

    def plot(self, fig):
        """
        function to generate Bokeh plot for web GUI.

        Also callable from an ipython notebook

        parameters
        ----------
        fig: object
            Bokeh bokeh.plotting.figure object

        returns
        -------
        fig: object
            Bokeh bokeh.plotting.figure object
        """
        try:
            # formatting
            fig.title = 'Energy production'
            fig.xaxis[0].axis_label = 'Wind speed [m/s]'
            fig.yaxis[0].axis_label = 'Energy production [kWh]'

            # fatigue, ultimate and final thickness line plots
            fig.line(self.wind_curve, self.aep_pr_wsp, line_color='orange',
                                        line_width=3)

            fig.legend[0].orientation = 'top_right'
        except:
            pass

        return fig


class SEAMAEP(Assembly):

    def configure(self):

        self.add('rotor_aero', SEAM_PowerCurve())
        self.add('aep_calc', SEAM_AEP())
        self.driver.workflow.add(['rotor_aero', 'aep_calc'])

        self.connect('rotor_aero.wind_curve', 'aep_calc.wind_curve')
        self.connect('rotor_aero.power_curve', 'aep_calc.power_curve')
        
        self.create_passthrough('rotor_aero.rated_torque')
        self.create_passthrough('rotor_aero.rated_speed')

        for name in self.rotor_aero.list_vars():

            try:
                self.create_passthrough('rotor_aero.%s' % name)
            except:
                pass

        for name in self.aep_calc.list_vars():

            try:
                self.create_passthrough('aep_calc.%s' % name)
            except:
                pass
